package com.artivisi.training.microservice.frontend.service;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Component;

@Component
public class WalletServiceFallback implements WalletService {

    @Override
    public Map<String, String> info() {
        Map<String, String> hasil = new HashMap<>();
        hasil.put("Local IP", "0.0.0.0");
        hasil.put("Local Port", "99999");
        hasil.put("Hostname", "NoServerAvailable");
        return hasil;
	}

}