package com.artivisi.training.microservice.frontend.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.RestTemplate;

@Controller
public class OAuthCallbackHandler {

    private String clientId = "client001";
    private String clientSecret = "abcd";
    private String tokenUrl = "http://localhost:8080/oauth/token";
    @Autowired
    private RestTemplateBuilder restTemplateBuilder;

    @GetMapping("/handle-oauth-callback")
    public ModelMap handleCallback(@RequestParam String code) {
        MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();
        map.add("grant_type", "authorization_code");
        map.add("redirect_uri", "http://localhost:10000/handle-oauth-callback");
        map.add("code", code);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);        

        HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<MultiValueMap<String, String>>(map, headers);

        RestTemplate rest = restTemplateBuilder
            .basicAuthorization(clientId, clientSecret)
            .build();
        
        Map<String, String> hasil = rest.postForObject(tokenUrl, request, HashMap.class);
        System.out.println("Access Token : "+hasil.get("access_token"));
        System.out.println("Refresh Token : "+hasil.get("refresh_token"));

        return new ModelMap()
        .addAttribute("access_token", hasil.get("access_token"))
        .addAttribute("refresh_token", hasil.get("refresh_token"));
    }
}