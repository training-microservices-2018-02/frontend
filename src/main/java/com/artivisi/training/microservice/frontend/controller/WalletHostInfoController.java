package com.artivisi.training.microservice.frontend.controller;

import java.util.Map;

import com.artivisi.training.microservice.frontend.service.WalletService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class WalletHostInfoController {
    @Autowired private WalletService walletService;

    @GetMapping("/wallet/info")
    public Map<String, String> walletInfo(){
        return walletService.info();
    }
}